# LAB5. Student: Aldiyar Amirov
## SCRIPT IS IN script.py FILE

## Specs for Me
Here is InnoCar Specs:\
Budet car price per minute = 20\
Luxury car price per minute = 51\
Fixed price per km = 13\
Allowed deviations in % = 6\
Inno discount in % = 14.000000000000002

## DVA
   | Parameter          | Equivalence Class                 |
   | ------------------ | --------------------------------- |
   | "distance"         | "<=0", ">0"                       |
   | "planned_distance" | "<=0", ">0"                       |
   | "time"             | "<=0", ">0"                       |
   | "planned_time"     | "<=0", ">0"                       |
   | "type"             | "budget", "luxury", nonsense      |
   | "plan"             | "minute", "fixed_price", nonsense |
   | "inno_discount"    | "yes", "no", nonsense             |

## Decision table
|TEST|type    |plan       |inno_discount|distance|planned_distance|time|planned_distance|expected         |actual            |is_equal|
|----|--------|-----------|-------------|--------|----------------|----|----------------|-----------------|------------------|--------|
|95  |nonsense|fixed_price|no           |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|191 |budget  |nonsense   |no           |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|207 |budget  |fixed_price|nonsense     |10      |10              |10  |10              |Invalid Request  |125               |False   |
|231 |budget  |fixed_price|no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|235 |budget  |fixed_price|no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|237 |budget  |fixed_price|no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|238 |budget  |fixed_price|no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|239 |budget  |fixed_price|no           |10      |10              |10  |10              |130              |125               |False   |
|271 |budget  |minute     |yes          |10      |10              |10  |10              |172.0            |164               |False   |
|287 |budget  |minute     |no           |10      |10              |10  |10              |200              |200               |False   |
|303 |luxury  |nonsense   |nonsense     |10      |10              |10  |10              |Invalid Request  |510               |False   |
|319 |luxury  |nonsense   |yes          |10      |10              |10  |10              |Invalid Request  |418.20000000000005|False   |
|335 |luxury  |nonsense   |no           |10      |10              |10  |10              |Invalid Request  |510               |False   |
|399 |luxury  |minute     |nonsense     |10      |10              |10  |10              |Invalid Request  |510               |False   |
|415 |luxury  |minute     |yes          |10      |10              |10  |10              |438.59999999999997|418.20000000000005|False   |
|431 |luxury  |minute     |no           |10      |10              |10  |10              |510              |510               |True    |
|432 |budget  |fixed_price|no           |10      |10              |10  |20              |200              |166.66666666666666|False   |

## BUGS FOUND
* When you pass gibberish into inno_discount field it still gives you some price (but expected "Invalid Request")
* Fixed price for budget happens to be 12.5 not 13
* The discount happens to be 18 percent, but not 14 as was stated in specs
* When you pass gibberish into plan field it still give you some price ONLY FOR LUXURY TYPE (but expected "Invalid Argument")
* When plan switches from fixed_price to minute, it gives some wrong price


## RAW RESULTS OF THE TESTINGS
|TEST|type    |plan       |inno_discount|distance|planned_distance|time|planned_distance|expected         |actual            |is_equal|
|----|--------|-----------|-------------|--------|----------------|----|----------------|-----------------|------------------|--------|
|0   |nonsense|nonsense   |nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|1   |nonsense|nonsense   |nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|2   |nonsense|nonsense   |nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|3   |nonsense|nonsense   |nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|4   |nonsense|nonsense   |nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|5   |nonsense|nonsense   |nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|6   |nonsense|nonsense   |nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|7   |nonsense|nonsense   |nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|8   |nonsense|nonsense   |nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|9   |nonsense|nonsense   |nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|10  |nonsense|nonsense   |nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|11  |nonsense|nonsense   |nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|12  |nonsense|nonsense   |nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|13  |nonsense|nonsense   |nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|14  |nonsense|nonsense   |nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|15  |nonsense|nonsense   |nonsense     |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|16  |nonsense|nonsense   |yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|17  |nonsense|nonsense   |yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|18  |nonsense|nonsense   |yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|19  |nonsense|nonsense   |yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|20  |nonsense|nonsense   |yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|21  |nonsense|nonsense   |yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|22  |nonsense|nonsense   |yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|23  |nonsense|nonsense   |yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|24  |nonsense|nonsense   |yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|25  |nonsense|nonsense   |yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|26  |nonsense|nonsense   |yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|27  |nonsense|nonsense   |yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|28  |nonsense|nonsense   |yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|29  |nonsense|nonsense   |yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|30  |nonsense|nonsense   |yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|31  |nonsense|nonsense   |yes          |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|32  |nonsense|nonsense   |no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|33  |nonsense|nonsense   |no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|34  |nonsense|nonsense   |no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|35  |nonsense|nonsense   |no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|36  |nonsense|nonsense   |no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|37  |nonsense|nonsense   |no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|38  |nonsense|nonsense   |no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|39  |nonsense|nonsense   |no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|40  |nonsense|nonsense   |no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|41  |nonsense|nonsense   |no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|42  |nonsense|nonsense   |no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|43  |nonsense|nonsense   |no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|44  |nonsense|nonsense   |no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|45  |nonsense|nonsense   |no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|46  |nonsense|nonsense   |no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|47  |nonsense|nonsense   |no           |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|48  |nonsense|fixed_price|nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|49  |nonsense|fixed_price|nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|50  |nonsense|fixed_price|nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|51  |nonsense|fixed_price|nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|52  |nonsense|fixed_price|nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|53  |nonsense|fixed_price|nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|54  |nonsense|fixed_price|nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|55  |nonsense|fixed_price|nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|56  |nonsense|fixed_price|nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|57  |nonsense|fixed_price|nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|58  |nonsense|fixed_price|nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|59  |nonsense|fixed_price|nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|60  |nonsense|fixed_price|nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|61  |nonsense|fixed_price|nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|62  |nonsense|fixed_price|nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|63  |nonsense|fixed_price|nonsense     |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|64  |nonsense|fixed_price|yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|65  |nonsense|fixed_price|yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|66  |nonsense|fixed_price|yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|67  |nonsense|fixed_price|yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|68  |nonsense|fixed_price|yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|69  |nonsense|fixed_price|yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|70  |nonsense|fixed_price|yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|71  |nonsense|fixed_price|yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|72  |nonsense|fixed_price|yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|73  |nonsense|fixed_price|yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|74  |nonsense|fixed_price|yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|75  |nonsense|fixed_price|yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|76  |nonsense|fixed_price|yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|77  |nonsense|fixed_price|yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|78  |nonsense|fixed_price|yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|79  |nonsense|fixed_price|yes          |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|80  |nonsense|fixed_price|no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|81  |nonsense|fixed_price|no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|82  |nonsense|fixed_price|no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|83  |nonsense|fixed_price|no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|84  |nonsense|fixed_price|no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|85  |nonsense|fixed_price|no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|86  |nonsense|fixed_price|no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|87  |nonsense|fixed_price|no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|88  |nonsense|fixed_price|no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|89  |nonsense|fixed_price|no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|90  |nonsense|fixed_price|no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|91  |nonsense|fixed_price|no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|92  |nonsense|fixed_price|no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|93  |nonsense|fixed_price|no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|94  |nonsense|fixed_price|no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|95  |nonsense|fixed_price|no           |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|96  |nonsense|minute     |nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|97  |nonsense|minute     |nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|98  |nonsense|minute     |nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|99  |nonsense|minute     |nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|100 |nonsense|minute     |nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|101 |nonsense|minute     |nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|102 |nonsense|minute     |nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|103 |nonsense|minute     |nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|104 |nonsense|minute     |nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|105 |nonsense|minute     |nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|106 |nonsense|minute     |nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|107 |nonsense|minute     |nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|108 |nonsense|minute     |nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|109 |nonsense|minute     |nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|110 |nonsense|minute     |nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|111 |nonsense|minute     |nonsense     |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|112 |nonsense|minute     |yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|113 |nonsense|minute     |yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|114 |nonsense|minute     |yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|115 |nonsense|minute     |yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|116 |nonsense|minute     |yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|117 |nonsense|minute     |yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|118 |nonsense|minute     |yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|119 |nonsense|minute     |yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|120 |nonsense|minute     |yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|121 |nonsense|minute     |yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|122 |nonsense|minute     |yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|123 |nonsense|minute     |yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|124 |nonsense|minute     |yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|125 |nonsense|minute     |yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|126 |nonsense|minute     |yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|127 |nonsense|minute     |yes          |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|128 |nonsense|minute     |no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|129 |nonsense|minute     |no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|130 |nonsense|minute     |no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|131 |nonsense|minute     |no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|132 |nonsense|minute     |no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|133 |nonsense|minute     |no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|134 |nonsense|minute     |no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|135 |nonsense|minute     |no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|136 |nonsense|minute     |no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|137 |nonsense|minute     |no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|138 |nonsense|minute     |no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|139 |nonsense|minute     |no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|140 |nonsense|minute     |no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|141 |nonsense|minute     |no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|142 |nonsense|minute     |no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|143 |nonsense|minute     |no           |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|144 |budget  |nonsense   |nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|145 |budget  |nonsense   |nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|146 |budget  |nonsense   |nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|147 |budget  |nonsense   |nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|148 |budget  |nonsense   |nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|149 |budget  |nonsense   |nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|150 |budget  |nonsense   |nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|151 |budget  |nonsense   |nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|152 |budget  |nonsense   |nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|153 |budget  |nonsense   |nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|154 |budget  |nonsense   |nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|155 |budget  |nonsense   |nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|156 |budget  |nonsense   |nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|157 |budget  |nonsense   |nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|158 |budget  |nonsense   |nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|159 |budget  |nonsense   |nonsense     |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|160 |budget  |nonsense   |yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|161 |budget  |nonsense   |yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|162 |budget  |nonsense   |yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|163 |budget  |nonsense   |yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|164 |budget  |nonsense   |yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|165 |budget  |nonsense   |yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|166 |budget  |nonsense   |yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|167 |budget  |nonsense   |yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|168 |budget  |nonsense   |yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|169 |budget  |nonsense   |yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|170 |budget  |nonsense   |yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|171 |budget  |nonsense   |yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|172 |budget  |nonsense   |yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|173 |budget  |nonsense   |yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|174 |budget  |nonsense   |yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|175 |budget  |nonsense   |yes          |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|176 |budget  |nonsense   |no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|177 |budget  |nonsense   |no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|178 |budget  |nonsense   |no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|179 |budget  |nonsense   |no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|180 |budget  |nonsense   |no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|181 |budget  |nonsense   |no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|182 |budget  |nonsense   |no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|183 |budget  |nonsense   |no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|184 |budget  |nonsense   |no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|185 |budget  |nonsense   |no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|186 |budget  |nonsense   |no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|187 |budget  |nonsense   |no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|188 |budget  |nonsense   |no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|189 |budget  |nonsense   |no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|190 |budget  |nonsense   |no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|191 |budget  |nonsense   |no           |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|192 |budget  |fixed_price|nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|193 |budget  |fixed_price|nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|194 |budget  |fixed_price|nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|195 |budget  |fixed_price|nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|196 |budget  |fixed_price|nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|197 |budget  |fixed_price|nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|198 |budget  |fixed_price|nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|199 |budget  |fixed_price|nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|200 |budget  |fixed_price|nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|201 |budget  |fixed_price|nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|202 |budget  |fixed_price|nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|203 |budget  |fixed_price|nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|204 |budget  |fixed_price|nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|205 |budget  |fixed_price|nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|206 |budget  |fixed_price|nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|207 |budget  |fixed_price|nonsense     |10      |10              |10  |10              |Invalid Request  |125               |False   |
|208 |budget  |fixed_price|yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|209 |budget  |fixed_price|yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|210 |budget  |fixed_price|yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|211 |budget  |fixed_price|yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|212 |budget  |fixed_price|yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|213 |budget  |fixed_price|yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|214 |budget  |fixed_price|yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|215 |budget  |fixed_price|yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|216 |budget  |fixed_price|yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|217 |budget  |fixed_price|yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|218 |budget  |fixed_price|yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|219 |budget  |fixed_price|yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|220 |budget  |fixed_price|yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|221 |budget  |fixed_price|yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|222 |budget  |fixed_price|yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|223 |budget  |fixed_price|yes          |10      |10              |10  |10              |111.8            |102.50000000000001|False   |
|224 |budget  |fixed_price|no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|225 |budget  |fixed_price|no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|226 |budget  |fixed_price|no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|227 |budget  |fixed_price|no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|228 |budget  |fixed_price|no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|229 |budget  |fixed_price|no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|230 |budget  |fixed_price|no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|231 |budget  |fixed_price|no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|232 |budget  |fixed_price|no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|233 |budget  |fixed_price|no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|234 |budget  |fixed_price|no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|235 |budget  |fixed_price|no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|236 |budget  |fixed_price|no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|237 |budget  |fixed_price|no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|238 |budget  |fixed_price|no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|239 |budget  |fixed_price|no           |10      |10              |10  |10              |130              |125               |False   |
|240 |budget  |minute     |nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|241 |budget  |minute     |nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|242 |budget  |minute     |nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|243 |budget  |minute     |nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|244 |budget  |minute     |nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|245 |budget  |minute     |nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|246 |budget  |minute     |nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|247 |budget  |minute     |nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|248 |budget  |minute     |nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|249 |budget  |minute     |nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|250 |budget  |minute     |nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|251 |budget  |minute     |nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|252 |budget  |minute     |nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|253 |budget  |minute     |nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|254 |budget  |minute     |nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|255 |budget  |minute     |nonsense     |10      |10              |10  |10              |Invalid Request  |200               |False   |
|256 |budget  |minute     |yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|257 |budget  |minute     |yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|258 |budget  |minute     |yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|259 |budget  |minute     |yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|260 |budget  |minute     |yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|261 |budget  |minute     |yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|262 |budget  |minute     |yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|263 |budget  |minute     |yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|264 |budget  |minute     |yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|265 |budget  |minute     |yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|266 |budget  |minute     |yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|267 |budget  |minute     |yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|268 |budget  |minute     |yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|269 |budget  |minute     |yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|270 |budget  |minute     |yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|271 |budget  |minute     |yes          |10      |10              |10  |10              |Invalid Request  |164               |False   |
|272 |budget  |minute     |no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|273 |budget  |minute     |no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|274 |budget  |minute     |no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|275 |budget  |minute     |no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|276 |budget  |minute     |no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|277 |budget  |minute     |no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|278 |budget  |minute     |no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|279 |budget  |minute     |no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|280 |budget  |minute     |no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|281 |budget  |minute     |no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|282 |budget  |minute     |no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|283 |budget  |minute     |no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|284 |budget  |minute     |no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|285 |budget  |minute     |no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|286 |budget  |minute     |no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|287 |budget  |minute     |no           |10      |10              |10  |10              |Invalid Request  |200               |False   |
|288 |luxury  |nonsense   |nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|289 |luxury  |nonsense   |nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|290 |luxury  |nonsense   |nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|291 |luxury  |nonsense   |nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|292 |luxury  |nonsense   |nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|293 |luxury  |nonsense   |nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|294 |luxury  |nonsense   |nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|295 |luxury  |nonsense   |nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|296 |luxury  |nonsense   |nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|297 |luxury  |nonsense   |nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|298 |luxury  |nonsense   |nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|299 |luxury  |nonsense   |nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|300 |luxury  |nonsense   |nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|301 |luxury  |nonsense   |nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|302 |luxury  |nonsense   |nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|303 |luxury  |nonsense   |nonsense     |10      |10              |10  |10              |Invalid Request  |510               |False   |
|304 |luxury  |nonsense   |yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|305 |luxury  |nonsense   |yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|306 |luxury  |nonsense   |yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|307 |luxury  |nonsense   |yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|308 |luxury  |nonsense   |yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|309 |luxury  |nonsense   |yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|310 |luxury  |nonsense   |yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|311 |luxury  |nonsense   |yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|312 |luxury  |nonsense   |yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|313 |luxury  |nonsense   |yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|314 |luxury  |nonsense   |yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|315 |luxury  |nonsense   |yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|316 |luxury  |nonsense   |yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|317 |luxury  |nonsense   |yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|318 |luxury  |nonsense   |yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|319 |luxury  |nonsense   |yes          |10      |10              |10  |10              |Invalid Request  |418.20000000000005|False   |
|320 |luxury  |nonsense   |no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|321 |luxury  |nonsense   |no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|322 |luxury  |nonsense   |no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|323 |luxury  |nonsense   |no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|324 |luxury  |nonsense   |no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|325 |luxury  |nonsense   |no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|326 |luxury  |nonsense   |no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|327 |luxury  |nonsense   |no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|328 |luxury  |nonsense   |no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|329 |luxury  |nonsense   |no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|330 |luxury  |nonsense   |no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|331 |luxury  |nonsense   |no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|332 |luxury  |nonsense   |no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|333 |luxury  |nonsense   |no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|334 |luxury  |nonsense   |no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|335 |luxury  |nonsense   |no           |10      |10              |10  |10              |Invalid Request  |510               |False   |
|336 |luxury  |fixed_price|nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|337 |luxury  |fixed_price|nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|338 |luxury  |fixed_price|nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|339 |luxury  |fixed_price|nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|340 |luxury  |fixed_price|nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|341 |luxury  |fixed_price|nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|342 |luxury  |fixed_price|nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|343 |luxury  |fixed_price|nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|344 |luxury  |fixed_price|nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|345 |luxury  |fixed_price|nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|346 |luxury  |fixed_price|nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|347 |luxury  |fixed_price|nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|348 |luxury  |fixed_price|nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|349 |luxury  |fixed_price|nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|350 |luxury  |fixed_price|nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|351 |luxury  |fixed_price|nonsense     |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|352 |luxury  |fixed_price|yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|353 |luxury  |fixed_price|yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|354 |luxury  |fixed_price|yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|355 |luxury  |fixed_price|yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|356 |luxury  |fixed_price|yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|357 |luxury  |fixed_price|yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|358 |luxury  |fixed_price|yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|359 |luxury  |fixed_price|yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|360 |luxury  |fixed_price|yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|361 |luxury  |fixed_price|yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|362 |luxury  |fixed_price|yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|363 |luxury  |fixed_price|yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|364 |luxury  |fixed_price|yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|365 |luxury  |fixed_price|yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|366 |luxury  |fixed_price|yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|367 |luxury  |fixed_price|yes          |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|368 |luxury  |fixed_price|no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|369 |luxury  |fixed_price|no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|370 |luxury  |fixed_price|no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|371 |luxury  |fixed_price|no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|372 |luxury  |fixed_price|no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|373 |luxury  |fixed_price|no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|374 |luxury  |fixed_price|no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|375 |luxury  |fixed_price|no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|376 |luxury  |fixed_price|no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|377 |luxury  |fixed_price|no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|378 |luxury  |fixed_price|no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|379 |luxury  |fixed_price|no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|380 |luxury  |fixed_price|no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|381 |luxury  |fixed_price|no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|382 |luxury  |fixed_price|no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|383 |luxury  |fixed_price|no           |10      |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|384 |luxury  |minute     |nonsense     |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|385 |luxury  |minute     |nonsense     |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|386 |luxury  |minute     |nonsense     |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|387 |luxury  |minute     |nonsense     |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|388 |luxury  |minute     |nonsense     |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|389 |luxury  |minute     |nonsense     |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|390 |luxury  |minute     |nonsense     |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|391 |luxury  |minute     |nonsense     |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|392 |luxury  |minute     |nonsense     |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|393 |luxury  |minute     |nonsense     |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|394 |luxury  |minute     |nonsense     |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|395 |luxury  |minute     |nonsense     |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|396 |luxury  |minute     |nonsense     |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|397 |luxury  |minute     |nonsense     |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|398 |luxury  |minute     |nonsense     |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|399 |luxury  |minute     |nonsense     |10      |10              |10  |10              |Invalid Request  |510               |False   |
|400 |luxury  |minute     |yes          |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|401 |luxury  |minute     |yes          |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|402 |luxury  |minute     |yes          |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|403 |luxury  |minute     |yes          |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|404 |luxury  |minute     |yes          |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|405 |luxury  |minute     |yes          |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|406 |luxury  |minute     |yes          |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|407 |luxury  |minute     |yes          |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|408 |luxury  |minute     |yes          |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|409 |luxury  |minute     |yes          |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|410 |luxury  |minute     |yes          |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|411 |luxury  |minute     |yes          |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|412 |luxury  |minute     |yes          |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|413 |luxury  |minute     |yes          |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|414 |luxury  |minute     |yes          |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|415 |luxury  |minute     |yes          |10      |10              |10  |10              |438.59999999999997|418.20000000000005|False   |
|416 |luxury  |minute     |no           |-10     |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|417 |luxury  |minute     |no           |-10     |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|418 |luxury  |minute     |no           |-10     |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|419 |luxury  |minute     |no           |-10     |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|420 |luxury  |minute     |no           |-10     |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|421 |luxury  |minute     |no           |-10     |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|422 |luxury  |minute     |no           |-10     |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|423 |luxury  |minute     |no           |-10     |10              |10  |10              |Invalid Request  |Invalid Request   |True    |
|424 |luxury  |minute     |no           |10      |-10             |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|425 |luxury  |minute     |no           |10      |-10             |-10 |10              |Invalid Request  |Invalid Request   |True    |
|426 |luxury  |minute     |no           |10      |-10             |10  |-10             |Invalid Request  |Invalid Request   |True    |
|427 |luxury  |minute     |no           |10      |-10             |10  |10              |Invalid Request  |Invalid Request   |True    |
|428 |luxury  |minute     |no           |10      |10              |-10 |-10             |Invalid Request  |Invalid Request   |True    |
|429 |luxury  |minute     |no           |10      |10              |-10 |10              |Invalid Request  |Invalid Request   |True    |
|430 |luxury  |minute     |no           |10      |10              |10  |-10             |Invalid Request  |Invalid Request   |True    |
|431 |luxury  |minute     |no           |10      |10              |10  |10              |510              |510               |True    |
|432 |budget  |fixed_price|no           |10      |10              |10  |20              |200              |166.66666666666666|False   |
