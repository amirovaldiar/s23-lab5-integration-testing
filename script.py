import requests

types = ['nonsense', 'budget', 'luxury']       
plans = ['nonsense', 'fixed_price', 'minute']  
discounts = ['nonsense', 'yes', 'no']          
distances = [-10, 10]                   
planned_distances = [-10, 10]           
times =  [-10, 10]                       
planned_times = [-10, 10]         

budget_ppm = 20
luxury_ppm = 51
fixed_ppkm = 13
allowed_deviations = 6
discount = 1-0.14

def is_valid(params):
    return params['type'] in ['budget', 'luxury'] \
            and params['plan'] in ['minute', 'fixed_price'] \
            and params['distance'] > 0 \
            and params['planned_distance'] > 0 \
            and params['time'] > 0 \
            and params['planned_time'] > 0 \
            and params['distance'] > 0 \
            and params['planned_distance'] > 0 \
            and params['time'] > 0 \
            and params['planned_time'] > 0 \
            and params['inno_discount'] in ['yes', 'no'] \
            and not (params['type'] == 'luxury' and params['plan'] == 'fixed_price')

def calculate_price(params):
    plan = calculate_plan(params)
    price_per_minute = calculate_price_per_minute(params)
    if plan == 'minute':
        return price_per_minute * params['time'] * calculate_discount(params)
    return fixed_ppkm * params['distance'] * calculate_discount(params)

def calculate_price_per_minute(params):
    price_per_minute = budget_ppm
    if params['type'] == 'luxury':
        price_per_minute = luxury_ppm
    return price_per_minute

def calculate_plan(params):
    deviation = calculate_deviation(params)
    if params['plan'] == 'minute' or deviation > allowed_deviations:
        return 'minute'
    return 'fixed_price'

def calculate_deviation(params):
    time_deviation = abs(((params['time'] - params['planned_time'])/params['time'])*100)
    distance_deviation = abs(((params['distance'] - params['planned_distance'])/params['distance'])*100)
    return max(time_deviation, distance_deviation)

def calculate_discount(params):
    return discount if params['inno_discount']=='yes' else 1
    
def max(a, b): 
    return a if a > b else b

def abs(a):
    return a if a > 0 else -a

def get_price(params):
    URL = f'https://script.google.com/macros/s/AKfycbxReuav_j4FMBmT7i6ozd5E9oVjBOC7pKFyp5VlKjTRqmn7hjoeTDuzSayzMzTKKi2W/exec?' + \
        f'service=calculatePrice&email=al.amirov@innopolis.university&' + \
        f'type={params["type"]}' + \
        f'&plan={params["plan"]}&' + \
        f'distance={params["distance"]}&' + \
        f'planned_distance={params["planned_distance"]}' + \
        f'&time={params["time"]}&' + \
        f'planned_time={params["planned_time"]}&' + \
        f'inno_discount={params["inno_discount"]}'

    response = requests.get(URL)
    if response.content.decode() == 'Invalid Request':
        return response.content.decode()
    return response.json()["price"]

def print_result(params, test_number, expected, actual, result):
    print(f'{test_number},{params["type"]},{params["plan"]},{params["inno_discount"]},{params["distance"]},{params["planned_distance"]},{params["time"]},{params["planned_time"]},{expected},{actual},{result}')

def main():
    print("TEST,type,plan,inno_discount,distance,planned_distance,time,planned_distance,expected,actual,is_equal")
    test_number = 0
    for type_ in types:
        for plan_ in plans:
            for discount_ in discounts:
                for distance_ in distances:
                    for planned_distance_ in distances:
                        for time_ in times:
                            for planned_time_ in planned_times:
                                params = {
                                    'type': type_,
                                    'plan': plan_,
                                    'inno_discount': discount_,
                                    'distance': distance_,
                                    'planned_distance': planned_distance_,
                                    'time': time_,
                                    'planned_time': planned_time_,
                                }

                                if is_valid(params):
                                    expected = calculate_price(params)
                                else:
                                    expected = "Invalid Request"
                                actual = get_price(params)
                                result = expected == actual

                                print_result(params, test_number, expected, actual, result)
                                test_number += 1
main()